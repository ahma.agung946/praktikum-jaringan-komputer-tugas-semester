﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class Server
{
    private static void Gamexe(object argument)
    {
        TcpClient client = (TcpClient)argument;

        try
        {
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());

            //float Pjawab;
            float firstStockNumber;
          

            Random randomAnswer = new Random();
            Random randomStock = new Random();

            string InitMenu = reader.ReadLine();
            if (InitMenu != "Y")
            {
                string menu1 = " \n Kirimkan dengan cepat faktor prima dari bilangan yang diberikan. \n 2. kirimkan faktor Prima dari yang terkecil \n";
                writer.WriteLine(menu1);
                writer.Flush();

                

                string confirm2 = reader.ReadLine();
                if (confirm2 != "T")
                {
                    Console.WriteLine(" There is a player out");
                }
            }

            firstStockNumber = randomStock.Next(90, 100);
            Console.WriteLine(" Bilangan prima terkirim ( digitDua ) : " + firstStockNumber);
            writer.WriteLine(firstStockNumber);
            writer.Flush();

            
        }
        catch (IOException)
        {
            Console.WriteLine(" Pemainan Bearkhir\n");
        }
    }

    public static void Main()
    {
        TcpListener listener = null;
        try
        {
            listener = new TcpListener(IPAddress.Parse("10.252.39.170"), 8080);
            listener.Start();

            Console.WriteLine("RANDOM PRIME FACTOR NUMERIC RACE \n 4210181016 / M Ulwan Shidqi A");

            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                Console.WriteLine("Ada Player Masuk...");
                Thread newThread = new Thread(Gamexe);

                newThread.Start(client);
            }
        }

        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        finally
        {
            if (listener != null)
            {
                listener.Stop();
            }
        }
    }
}